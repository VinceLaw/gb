﻿using GBSharpClasses.Helpers;

namespace GBSharpClasses
{
    public class Stack
    {
         public void PushByte(byte value)
         {
             CPU.Get.Memory.WriteByte(--CPU.Get.Registers.SP.Value, value);
         }

         public byte PopByte()
         {
             return CPU.Get.Memory.ReadByte(CPU.Get.Registers.SP.Value++);
         }

         public void PushWord(short value)
         {
             CPU.Get.Memory.WriteWord(CPU.Get.Registers.SP.Value, value);
             CPU.Get.Registers.SP.Value -= 2;
         }

         public short PopWord()
         {
             var result = CPU.Get.Memory.ReadWord(CPU.Get.Registers.SP.Value);
             CPU.Get.Registers.SP.Value += 2;
             return result;
         }
    }
}