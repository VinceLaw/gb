using System.Collections.Generic;
using GBSharpClasses.RegisterClasses;

namespace GBSharpClasses
{
    public class Registers
    {
        private readonly Dictionary<char, Register8> R8 = new Dictionary<char, Register8>();

        private readonly Dictionary<string, Register16> R16 = new Dictionary<string, Register16>();

        public byte this[char name]
        {
            get { return R8[name].Value; }
            set { R8[name].Value = value; }
        }

        public short this[string name]
        {
            get { return R16[name].Value; }
            set { R16[name].Value = value; }
        }

        public Registers()
        {
            InitializeCommonRegisters();
            InitializeCombinedRegisters();
            InitializeFlags();
        }

        private void InitializeFlags()
        {
            IME = new Flag();
            Stop = new Flag();
            Halt = new Flag();
        }

        private void InitializeCommonRegisters()
        {
            InitializeInterruptRegisters();
            Initialize8BitRegisters();
            InitializeSystemRegisters();
            InitializeFlagsRegister();
            InitializeR8List();
            InitializeR16List();
        }

        private void InitializeSystemRegisters()
        {
            PC = new Register16();
            SP = new Register16();
        }

        private void Initialize8BitRegisters()
        {
            A = new Register8();
            B = new Register8();
            C = new Register8();
            D = new Register8();
            E = new Register8();
            H = new Register8();
            L = new Register8();
        }

        private void InitializeInterruptRegisters()
        {
            IF = new Register8();
            IE = new Register8();
            IME = new Flag();
            InitializeInterruptFlags();
        }

        private void InitializeInterruptFlags()
        {
            InitializeIFFlags();
            InitializeIEFlags();
        }

        private void InitializeIEFlags()
        {
            IE.AddFlag('J', 4);
            IE.AddFlag('S', 3);
            IE.AddFlag('T', 2);
            IE.AddFlag('L', 1);
            IE.AddFlag('V', 0);
        }

        private void InitializeIFFlags()
        {
            IF.AddFlag('J', 4);
            IF.AddFlag('S', 3);
            IF.AddFlag('T', 2);
            IF.AddFlag('L', 1);
            IF.AddFlag('V', 0);
        }

        private void InitializeR16List()
        {
            R16.Add("PC", PC);
            R16.Add("SP", SP);
            R16.Add("AF", AF);
            R16.Add("BC", BC);
            R16.Add("DE", DE);
            R16.Add("HL", HL);
        }

        private void InitializeR8List()
        {
            R8.Add('A', A);
            R8.Add('B', B);
            R8.Add('C', C);
            R8.Add('D', D);
            R8.Add('E', E);
            R8.Add('H', H);
            R8.Add('L', L);
        }

        private void InitializeCombinedRegisters()
        {
            AF = new Register16(F, A);
            BC = new Register16(C, B);
            DE = new Register16(E, D);
            HL = new Register16(L, H);
        }

        private void InitializeFlagsRegister()
        {
            F = new Register8();
            F.AddFlag('Z', 7);
            F.AddFlag('N', 6);
            F.AddFlag('H', 5);
            F.AddFlag('C', 4);
        }

        public void Inc(char reg8)
        {
            R8[reg8].Inc();
        }

        public void Inc(string reg16)
        {
            R16[reg16].Inc();
        }

        public void Dec(char reg8)
        {
            R8[reg8].Dec();
        }

        public void Dec(string reg16)
        {
            R16[reg16].Dec();
        }

        #region Flags
        public Flag Halt { get; set; }

        public Flag IME { get; set; }

        public Flag Stop { get; set; }
        #endregion

        #region Common registers
        public Register8 A { get; set; }

        public Register8 B { get; set; }

        public Register8 C { get; set; }

        public Register8 D { get; set; }

        public Register8 E { get; set; }

        public Register8 F { get; set; }

        public Register8 H { get; set; }

        public Register8 L { get; set; }

        public Register16 PC { get; set; }

        public Register16 SP { get; set; }

        public Register8 IF { get; set; }

        public Register8 IE { get; set; }
        #endregion

        #region CombinedRegisters
        public Register16 AF { get; set; }

        public Register16 BC { get; set; }

        public Register16 DE { get; set; }

        public Register16 HL { get; set; }
        #endregion

        public void Reset()
        {
            IME.Value = 1;
            Stop.Value = Halt.Value = 0;
            A.Value = B.Value = C.Value = D.Value = E.Value = F.Value = 0;
            PC.Value = SP.Value = 0;
        }

        public void Add(string destination, string source)
        {
            R16[destination].Add(R16[source].Value);
        }

        public void Add(char reg8, byte value)
        {
            R8[reg8].Add(value);
        }

        public void Add(string reg16, short value)
        {
            R16[reg16].Add(value);
        }
    }
}