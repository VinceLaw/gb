namespace GBSharpClasses.Helpers
{
    public class Singleton<T> where T : class, new()
    {
        protected Singleton()
        {
        }

        private static object locker = new object();
        private static volatile T instance;

        public static T Get
        {
            get
            {
                if (instance == null)
                {
                    lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = new T();
                        }
                    }
                }
                return instance;
            }
        }
    }
}