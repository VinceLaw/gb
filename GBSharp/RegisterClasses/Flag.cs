namespace GBSharpClasses.RegisterClasses
{
    public class Flag
    {
        public byte Value { get; set; }

        public Flag(byte value)
        {
            Value = value;
        }

        public Flag()
        {
            Value = 0;
        }
    }
}