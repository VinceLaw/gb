namespace GBSharpClasses.RegisterClasses
{
    public class Register16 : AbstractRegister<short>
    {
        public Register8 Low { get; set; }

        public Register8 High { get; set; }

        public short Value
        {
            get
            {
                return (short)((Low.Value & 0xffff) | (short)(High.Value << 8));
            }
            set
            {
                Low.Value = (byte)(value >> 8);
                High.Value = (byte)(value & 0xffff);
            }
        }

        public override short GetValue()
        {
            return Value;
        }

        public override void SetValue(short value)
        {
            Value = value;
        }

        public override byte GetBit(byte index)
        {
            return (byte)((Value & (1 << index - 1)) != 0 ? 1 : 0);
        }

        public override void SetBit(byte index, byte bit)
        {
            Value = bit == 0 ? (short)(Value & (~(1 << bit))) : (short)(Value | (1 << bit));
        }

        public Register16(Register8 low, Register8 high)
        {
            Low = low;
            High = high;
        }

        public Register16()
        {
            Low = new Register8();
            High = new Register8();
        }

        public override void Add(short secondValue, bool withCarry = false)
        {
            short oldValue = GetValue();
            int summ = oldValue + secondValue + (withCarry ? CPU.Get.Registers.F['C'] : 0);
            short newValue = (short)summ;
            CPU.Get.Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
            CPU.Get.Registers.F['H'] = (byte)((oldValue < 256 && newValue >= 256) ? 1 : 0);
            CPU.Get.Registers.F['N'] = 0;
            CPU.Get.Registers.F['C'] = (byte)(summ > short.MaxValue ? 1 : 0);
            Value = newValue;
        }

        public override void Inc()
        {
            Value = (short) (GetValue() + 1);
        }

        public override void Dec()
        {
            Value = (short)(GetValue() - 1);
        }
    }
}