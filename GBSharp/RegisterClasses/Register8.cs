namespace GBSharpClasses.RegisterClasses
{
    public class Register8 : AbstractRegister<byte>
    {
        public byte Value { get; set; }

        public override byte GetValue()
        {
            return Value;
        }

        public override void SetValue(byte value)
        {
            Value = value;
        }

        public override byte GetBit(byte index)
        {
            return (byte)((Value & (1 << index - 1)) != 0 ? 1 : 0);
        }

        public override void SetBit(byte index, byte bit)
        {
            Value = bit == 0 ? (byte) (Value & (~(1 << bit))) : (byte) (Value | (1 << bit));
        }

        public override void Add(byte secondValue, bool withCarry = false)
        {
            byte oldValue = GetValue();
            int summ = oldValue + secondValue + (withCarry ? CPU.Get.Registers.F['C'] : 0);
            byte newValue = (byte) summ;
            CPU.Get.Registers.F['Z'] = (byte) (newValue == 0 ? 1 : 0);
            CPU.Get.Registers.F['H'] = (byte) ((oldValue < 16 && newValue >= 16) ? 1 : 0);
            CPU.Get.Registers.F['N'] = 0;
            CPU.Get.Registers.F['C'] = (byte) (summ > byte.MaxValue ? 1 : 0);
            Value = newValue;
        }

        public void Sub(byte secondValue, bool withCarry = false, bool remember = true)
        {
            byte oldValue = GetValue();
            int summ = oldValue - secondValue - (withCarry ? CPU.Get.Registers.F['C'] : 0);
            byte newValue = (byte)summ;
            CPU.Get.Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
            CPU.Get.Registers.F['H'] = (byte)((oldValue >= 16 && newValue < 16) ? 0 : 1);
            CPU.Get.Registers.F['N'] = 1;
            CPU.Get.Registers.F['C'] = (byte)(summ < 0 ? 1 : 0);
            Value = newValue;
        }

        public void Cmp(byte secondValue)
        {
            Sub(secondValue, false, false);
        }

        public override void Dec()
        {
            byte oldValue = GetValue();
            int summ = oldValue - 1;
            byte newValue = (byte)summ;
            CPU.Get.Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
            CPU.Get.Registers.F['N'] = 1;
            Value = newValue;
        }

        public override void Inc()
        {
            byte oldValue = GetValue();
            int summ = oldValue + 1;
            byte newValue = (byte)summ; 
            CPU.Get.Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
            CPU.Get.Registers.F['N'] = 0;
            CPU.Get.Registers.F['C'] = (byte)(summ > byte.MaxValue ? 1 : 0);
            Value = newValue;
        }
    }
}