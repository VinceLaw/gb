using System.Collections.Generic;

namespace GBSharpClasses.RegisterClasses
{
    public abstract class AbstractRegister<T>
    {
        private readonly Dictionary<char, byte> flags = new Dictionary<char, byte>();

        public abstract T GetValue();

        public abstract void SetValue(T value);

        public abstract byte GetBit(byte index);

        public abstract void SetBit(byte index, byte bit);

        public abstract void Inc();

        public abstract void Dec();

        public abstract void Add(T secondValue, bool withCarry = false);
        
        public void AddFlag(char name, byte index)
        {
            flags.Add(name, index);
        }
        
        public byte this[byte index]
        {
            get { return GetBit(index); }
            set { SetBit(index, value); }
        }

        public byte this[char flag]
        {
            get { return GetBit(flags[flag]); }
            set { SetBit(flags[flag], value); }
        }

        protected AbstractRegister(Dictionary<char, byte> flags)
        {
            this.flags = flags;
        }

        protected AbstractRegister()
        {
            flags = new Dictionary<char, byte>();
        }

        public void SetValue(AbstractRegister<T> register)
        {
            SetValue(register.GetValue());
        }
    }
}