namespace GBSharpClasses
{
    public class MMU
    {
        public bool BIOSToBeLoaded { get; set; }
        
        public byte[] ZeroPage { get; set; }

        public byte[] ROM { get; set; }

        public byte[] eRAM { get; set; }

        public byte[] wRAM { get; set; }

        public byte[] BIOS { get; set; }

        public MMU()
        {
            BIOSToBeLoaded = true;
            BIOS = new byte[]
                {
                    0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C,
                    0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E, 0x11, 0x3E, 0x80, 0x32,
                    0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E,
                    0xFC, 0xE0, 0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A,
                    0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B, 0xFE, 0x34,
                    0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06, 0x08, 0x1A, 0x13, 0x22,
                    0x23, 0x05, 0x20, 0xF9, 0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21,
                    0x2F, 0x99, 0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
                    0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64, 0x57, 0xE0,
                    0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04, 0x1E, 0x02, 0x0E, 0x0C,
                    0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D,
                    0x20, 0xF2, 0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62,
                    0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06, 0x7B, 0xE2,
                    0x0C, 0x3E, 0x87, 0xF2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15,
                    0x20, 0xD2, 0x05, 0x20, 0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F,
                    0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
                    0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0xCE, 0xED,
                    0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83,
                    0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89,
                    0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
                    0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC,
                    0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0x3c, 0x42, 0xB9, 0xA5,
                    0xB9, 0xA5, 0x42, 0x4C, 0x21, 0x04, 0x01, 0x11, 0xA8, 0x00,
                    0x1A, 0x13, 0xBE, 0x20, 0xFE, 0x23, 0x7D, 0xFE, 0x34, 0x20,
                    0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86,
                    0x20, 0xFE, 0x3E, 0x01, 0xE0, 0x50
                };
            ROM = new byte[0x4000];
        }

        public virtual void WriteByte(short address, byte value)
        {
            switch (address & 0xF000)
            {
                // BIOS (256b) ��� ROM0
                case 0x0000:
                    if (BIOSToBeLoaded)
                    {
                        if (address < 0x0100)
                        {
                            BIOS[address] = value;
                            break;
                        }
                        if (CPU.Get.Registers.PC.Value == 0x0100)
                        {
                            BIOSToBeLoaded = false;
                        }
                    }
                    ROM[address] = value;
                    break;

                // ROM0
                case 0x1000:
                case 0x2000:
                case 0x3000:
                    {
                        ROM[address] = value;
                        break;
                    }

                // ROM1 (unbanked) (16k)
                case 0x4000:
                case 0x5000:
                case 0x6000:
                case 0x7000:
                    {
                        ROM[address] = value;
                        break;
                    }

                // Graphics: VRAM (8k)
                case 0x8000:
                case 0x9000:
                    {
                        CPU.Get.GPU.vRAM[address & 0x1FFF] = value;
                        break;
                    }

                // External RAM (8k)
                case 0xA000:
                case 0xB000:
                    {
                        eRAM[address & 0x1FFF] = value;
                        break;
                    }

                // ������� RAM (8k)
                case 0xC000:
                case 0xD000:
                    {
                        wRAM[address & 0x1FFF] = value;
                        break;
                    }

                // ������� ������� RAM
                case 0xE000:
                    {
                        wRAM[address & 0x1FFF] = value;
                        break;
                    }

                // ������� ������� RAM, ����/�����, Zero-page RAM
                case 0xF000:
                    switch (address & 0x0F00)
                    {
                        // ������� ������� Working RAM
                        case 0x000:
                        case 0x100:
                        case 0x200:
                        case 0x300:
                        case 0x400:
                        case 0x500:
                        case 0x600:
                        case 0x700:
                        case 0x800:
                        case 0x900:
                        case 0xA00:
                        case 0xB00:
                        case 0xC00:
                        case 0xD00:
                            {
                                wRAM[address & 0x1FFF] = value;
                                break;
                            }

                        // �������: object attribute memory (������ ��������� �������)
                        // ������ OAM 160 ����. ���������� ����� �������� ��� 0
                        case 0xE00:
                            {
                                if (address < 0xFEA0)
                                {
                                    CPU.Get.GPU.OAM[address & 0xFF] = value;
                                }
                                break;
                            }

                            // Zero-page
                        case 0xF00:
                            if (address == 0xFFFF)
                            {
                                CPU.Get.Registers.IE.Value = value;
                            }
                            if (address >= 0xFF80)
                            {
                                ZeroPage[address & 0x7F] = value;
                            }
                            else
                            {
                                // ��������� �����/������
                                switch (address & 0x00F0)
                                {
                                    case 0x00:
                                        if (address == 0xFF0F)
                                        {
                                            CPU.Get.Registers.IF.Value = value;
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                    break;
            }
        }

        public virtual byte ReadByte(short address)
        {
            switch (address & 0xF000)
            {
                // BIOS (256b) ��� ROM0
                case 0x0000:
                    if (BIOSToBeLoaded)
                    {
                        if (address < 0x0100)
                        {
                            return BIOS[address];
                        }
                        if (CPU.Get.Registers.PC.Value == 0x0100)
                        {
                            BIOSToBeLoaded = false;
                        }
                    }
                    return ROM[address];

                // ROM0
                case 0x1000:
                case 0x2000:
                case 0x3000:
                    {
                        return ROM[address];
                    }

                // ROM1 (unbanked) (16k)
                case 0x4000:
                case 0x5000:
                case 0x6000:
                case 0x7000:
                    {
                        return ROM[address];
                    }

                // Graphics: VRAM (8k)
                case 0x8000:
                case 0x9000:
                    return CPU.Get.GPU.vRAM[address & 0x1FFF];

                // External RAM (8k)
                case 0xA000:
                case 0xB000:
                    return eRAM[address & 0x1FFF];

                // ������� RAM (8k)
                case 0xC000:
                case 0xD000:
                    return wRAM[address & 0x1FFF];

                // ������� ������� RAM
                case 0xE000:
                    return wRAM[address & 0x1FFF];

                // ������� ������� RAM, ����/�����, Zero-page RAM
                case 0xF000:
                    switch (address & 0x0F00)
                    {
                        // ������� ������� Working RAM
                        case 0x000:
                        case 0x100:
                        case 0x200:
                        case 0x300:
                        case 0x400:
                        case 0x500:
                        case 0x600:
                        case 0x700:
                        case 0x800:
                        case 0x900:
                        case 0xA00:
                        case 0xB00:
                        case 0xC00:
                        case 0xD00:
                            return wRAM[address & 0x1FFF];

                        // �������: object attribute memory (������ ��������� �������)
                        // ������ OAM 160 ����. ���������� ����� �������� ��� 0
                        case 0xE00:
                            return address < 0xFEA0 ? CPU.Get.GPU.OAM[address & 0xFF] : (byte)0;

                        // Zero-page
                        case 0xF00:
                            if (address == 0xFFFF)
                            {
                                return CPU.Get.Registers.IE.Value;
                            }
                            if (address >= 0xFF80)
                            {
                                return ZeroPage[address & 0x7F];
                            }
                            else
                            {
                                // ��������� �����/������
                                switch (address & 0x00F0)
                                {
                                    case 0x00:
                                        if (address == 0xFF0F)
                                        {
                                            return CPU.Get.Registers.IF.Value;
                                        }
                                        break;
                                }
                                return 0;
                            }
                    }
                    break;
            }
            return 0;
        }

        public virtual void WriteWord(short address, short value)
        {
            var low = (byte)(value >> 8);
            var high = (byte)(value & 0xffff);

            WriteByte(address, low);
            WriteByte((short)(address + 1), high);
        }

        public virtual short ReadWord(short address)
        {
            return (short)(ReadByte(address) + ReadByte((short) ((address + 1) << 8)));
        }
    }
}