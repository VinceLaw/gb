﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using GBSharpClasses.Enumarations;
using GBSharpClasses.Helpers;

namespace GBSharpClasses
{
    public class CPU : Singleton<CPU>
    {
        public const int Frequency = 4194304;

        private readonly Dictionary<int, Command> commands = new Dictionary<int, Command>();

        private readonly Dictionary<int, Command> commandsExtended = new Dictionary<int, Command>();

        public short M { get; set; }

        public short Cycles { get; set; }

        public GPU GPU { get; set; }

        public MMU Memory { get; set; }

        public Registers Registers { get; set; }

        public Stack Stack { get; set; }

        public Timer MTimer { get; set; }

        public Timer TTimer { get; set; }

        public InterruptsController IntControl { get; set; }
        
        public CPU()
        {
            Registers = new Registers();
            Memory = new MMU();
            Stack = new Stack();
            GPU = new GPU();
            //TODO : i flag?
            //TODO : r flag?
            MTimer = new Timer();
            TTimer = new Timer();
            InitializeCommands();
        }

        public void Reset()
        {
            Registers.Reset();
            // TODO : reset i flag?
            // TODO : reset r flag?
            M = Cycles = 0;
            TTimer.Value = MTimer.Value = 0;
        }

        public void Run()
        {
            while (true)
            {
                var op = Memory.ReadByte(Registers.PC.Value++);
                if (op == 0xCB)
                {
                    op = Memory.ReadByte(Registers.PC.Value++);
                    commandsExtended[op].Execute();
                }
                else
                {
                    commands[op].Execute();
                }
                TTimer.Value += Cycles;
                MTimer.Value += M;

                IntControl.Process();
                GPU.Update();
            }
        }

        public void LoadROM(string filename)
        {
            byte[] fileBytes = File.ReadAllBytes(filename);
            for(int i = 0; i < fileBytes.Length; i++)
            {
                Memory.ROM[i] = fileBytes[i];
            }
        }

        private void InitializeCommands()
        {
            Com(0x00, 1, 4, "NOP", () => { });
            Com(0x7F, 1, 4, "LD A, A", () => MoveReg8ToReg8('A', 'A'));
            Com(0x3E, 2, 8, "LD A, n", () => MoveNextByteToReg8('A'));
            Com(0x78, 1, 4, "LD A, B", () => MoveReg8ToReg8('A', 'B'));
            Com(0x79, 1, 4, "LD A, C", () => MoveReg8ToReg8('A', 'C'));
            Com(0x7A, 1, 4, "LD A, D", () => MoveReg8ToReg8('A', 'D'));
            Com(0x7B, 1, 4, "LD A, E", () => MoveReg8ToReg8('A', 'E'));
            Com(0x7C, 1, 4, "LD A, H", () => MoveReg8ToReg8('A', 'H'));
            Com(0x7D, 1, 4, "LD A, L", () => MoveReg8ToReg8('A', 'L'));
            Com(0x7E, 2, 8, "LD A, (HL)", () => MoveMemoryByteFromReg16ToReg8('A', "HL"));
            Com(0x0A, 2, 8, "LD A, (BC)", () => MoveMemoryByteFromReg16ToReg8('A', "BC"));
            Com(0x7E, 2, 8, "LD A, (DE)", () => MoveMemoryByteFromReg16ToReg8('A', "DE"));
            Com(0xFA, 4, 16, "LD A, (nn)", () => MoveMemoryByteFromNextWordToReg8('A'));
            Com(0xF2, 2, 8, "LD A, ($FF00 + C)", () => MoveMemoryByteFromFF00PlusCToReg8('A'));
            Com(0x47, 1, 4, "LD B, A", () => MoveReg8ToReg8('B', 'A'));
            Com(0x40, 1, 4, "LD B, B", () => MoveReg8ToReg8('B', 'B'));
            Com(0x41, 1, 4, "LD B, C", () => MoveReg8ToReg8('B', 'C'));
            Com(0x42, 1, 4, "LD B, D", () => MoveReg8ToReg8('B', 'D'));
            Com(0x43, 1, 4, "LD B, E", () => MoveReg8ToReg8('B', 'E'));
            Com(0x44, 1, 4, "LD B, H", () => MoveReg8ToReg8('B', 'H'));
            Com(0x45, 1, 4, "LD B, L", () => MoveReg8ToReg8('B', 'L'));
            Com(0x46, 2, 8, "LD B, HL", () => MoveMemoryByteFromReg16ToReg8('B', "HL"));
            Com(0xE2, 2, 8, "LD ($FF00 + C), A", () => MoveReg8ToMemoryByteFromFF00PlusC('A'));
            Com(0x4F, 1, 4, "LD C, A", () => MoveReg8ToReg8('C', 'A'));
            Com(0x48, 1, 4, "LD C, B", () => MoveReg8ToReg8('C', 'B'));
            Com(0x49, 1, 4, "LD C, C", () => MoveReg8ToReg8('C', 'C'));
            Com(0x4A, 1, 4, "LD C, D", () => MoveReg8ToReg8('C', 'D'));
            Com(0x4B, 1, 4, "LD C, E", () => MoveReg8ToReg8('C', 'E'));
            Com(0x4C, 1, 4, "LD C, H", () => MoveReg8ToReg8('C', 'H'));
            Com(0x4D, 1, 4, "LD C, L", () => MoveReg8ToReg8('C', 'L'));
            Com(0x4E, 2, 8, "LD C, (HL)", () => MoveMemoryByteFromReg16ToReg8('C', "HL"));
            Com(0x57, 1, 4, "LD D, A", () => MoveReg8ToReg8('D', 'A'));
            Com(0x50, 1, 4, "LD D, B", () => MoveReg8ToReg8('D', 'B'));
            Com(0x51, 1, 4, "LD D, C", () => MoveReg8ToReg8('D', 'C'));
            Com(0x52, 1, 4, "LD D, D", () => MoveReg8ToReg8('D', 'D'));
            Com(0x53, 1, 4, "LD D, E", () => MoveReg8ToReg8('D', 'E'));
            Com(0x54, 1, 4, "LD D, H", () => MoveReg8ToReg8('D', 'H'));
            Com(0x55, 1, 4, "LD D, L", () => MoveReg8ToReg8('D', 'L'));
            Com(0x56, 2, 8, "LD D, HL", () => MoveMemoryByteFromReg16ToReg8('C', "HL"));
            Com(0x5F, 1, 4, "LD E, A", () => MoveReg8ToReg8('E', 'A'));
            Com(0x58, 1, 4, "LD E, B", () => MoveReg8ToReg8('E', 'B'));
            Com(0x59, 1, 4, "LD E, C", () => MoveReg8ToReg8('E', 'C'));
            Com(0x5A, 1, 4, "LD E, D", () => MoveReg8ToReg8('E', 'D'));
            Com(0x5B, 1, 4, "LD E, E", () => MoveReg8ToReg8('E', 'E'));
            Com(0x5C, 1, 4, "LD E, H", () => MoveReg8ToReg8('E', 'H'));
            Com(0x5D, 1, 4, "LD E, L", () => MoveReg8ToReg8('E', 'L'));
            Com(0x5E, 2, 8, "LD E, (HL)", () => MoveMemoryByteFromReg16ToReg8('E', "HL"));
            Com(0x67, 1, 4, "LD H, A", () => MoveReg8ToReg8('H', 'A'));
            Com(0x60, 1, 4, "LD H, B", () => MoveReg8ToReg8('H', 'B'));
            Com(0x61, 1, 4, "LD H, C", () => MoveReg8ToReg8('H', 'C'));
            Com(0x62, 1, 4, "LD H, D", () => MoveReg8ToReg8('H', 'D'));
            Com(0x63, 1, 4, "LD H, E", () => MoveReg8ToReg8('H', 'E'));
            Com(0x64, 1, 4, "LD H, H", () => MoveReg8ToReg8('H', 'H'));
            Com(0x65, 1, 4, "LD H, L", () => MoveReg8ToReg8('H', 'L'));
            Com(0x66, 2, 8, "LD H, (HL)", () => MoveMemoryByteFromReg16ToReg8('H', "HL"));
            Com(0x6F, 1, 4, "LD L, A", () => MoveReg8ToReg8('L', 'A'));
            Com(0x68, 1, 4, "LD L, B", () => MoveReg8ToReg8('L', 'B'));
            Com(0x69, 1, 4, "LD L, C", () => MoveReg8ToReg8('L', 'C'));
            Com(0x6A, 1, 4, "LD L, D", () => MoveReg8ToReg8('L', 'D'));
            Com(0x6B, 1, 4, "LD L, E", () => MoveReg8ToReg8('L', 'E'));
            Com(0x6C, 1, 4, "LD L, H", () => MoveReg8ToReg8('L', 'H'));
            Com(0x6D, 1, 4, "LD L, L", () => MoveReg8ToReg8('L', 'L'));
            Com(0x6E, 2, 8, "LD L, (HL)", () => MoveMemoryByteFromReg16ToReg8('L', "HL"));
            Com(0x06, 2, 8, "LD B, n", () => MoveNextByteToReg8('B'));
            Com(0x0E, 2, 8, "LD C, n", () => MoveNextByteToReg8('C'));
            Com(0x16, 2, 8, "LD D, n", () => MoveNextByteToReg8('D'));
            Com(0x1E, 2, 8, "LD E, n", () => MoveNextByteToReg8('E'));
            Com(0x26, 2, 8, "LD H, n", () => MoveNextByteToReg8('H'));
            Com(0x2E, 2, 8, "LD L, n", () => MoveNextByteToReg8('L'));
            Com(0x36, 3, 12, "LD (HL), n", () => MoveNextByteToMemoryByteFromReg16("HL"));
            Com(0x77, 2, 8, "LD (HL), A", () => MoveReg8ToMemoryByteFromReg16("HL", 'A'));
            Com(0x70, 2, 8, "LD (HL), B", () => MoveReg8ToMemoryByteFromReg16("HL", 'B'));
            Com(0x71, 2, 8, "LD (HL), C", () => MoveReg8ToMemoryByteFromReg16("HL", 'C'));
            Com(0x72, 2, 8, "LD (HL), D", () => MoveReg8ToMemoryByteFromReg16("HL", 'D'));
            Com(0x73, 2, 8, "LD (HL), E", () => MoveReg8ToMemoryByteFromReg16("HL", 'E'));
            Com(0x74, 2, 8, "LD (HL), H", () => MoveReg8ToMemoryByteFromReg16("HL", 'H'));
            Com(0x75, 2, 8, "LD (HL), L", () => MoveReg8ToMemoryByteFromReg16("HL", 'L'));
            Com(0x02, 2, 8, "LD (BC), A", () => MoveReg8ToMemoryByteFromReg16("BC", 'A'));
            Com(0x12, 2, 8, "LD (DE), A", () => MoveReg8ToMemoryByteFromReg16("DE", 'A'));
            Com(0xEA, 4, 16, "LD (nn), A", () => MoveReg8ToMemoryByteFromNextWord('A'));
            Com(0x3A, 2, 8, "LDD A, (HL)", () => MoveMemoryByteFromReg16ToReg8('A', "HL", AddMode.Dec));
            Com(0x32, 2, 8, "LDD (HL), A", () => MoveReg8ToMemoryByteFromReg16("HL", 'A', AddMode.Dec));
            Com(0x2A, 2, 8, "LDI A, (HL)", () => MoveMemoryByteFromReg16ToReg8('A', "HL", AddMode.Inc));
            Com(0x22, 2, 8, "LDI (HL), A", () => MoveReg8ToMemoryByteFromReg16("HL", 'A', AddMode.Inc));
            Com(0xE0, 3, 12, "LD ($FF00 + n), A", () => MoveReg8ToMemoryByteFromFF00PlusNextByte('A'));
            Com(0xF0, 3, 12, "LD A, ($FF00 + n)", () => MoveMemoryByteFromFF00PlusNextByteToReg8('A'));
            Com(0x01, 3, 12, "LD BC, nn", () => MoveNextWordToReg16("BC"));
            Com(0x31, 3, 12, "LD SP, nn", () => MoveNextWordToReg16("SP"));
            Com(0x11, 3, 12, "LD DE, nn", () => MoveNextWordToReg16("DE"));
            Com(0x21, 3, 12, "LD HL, nn", () => MoveNextWordToReg16("HL"));
            Com(0xF9, 2, 8, "LD SP, HL", () => MoveReg16ToReg16("SP", "HL"));
            Com(0xF8, 2, 8, "LD HL, SP + n", () => MoveReg16PlusNextByteToReg16("HL", "SP"));
            Com(0x08, 2, 8, "LD (nn), SP", () => MoveReg16ToMemoryByteFromNextWord("SP"));
            Com(0xF5, 4, 16, "PUSH AF", () => PushReg16("AF"));
            Com(0xC5, 4, 16, "PUSH BC", () => PushReg16("BC"));
            Com(0xD5, 4, 16, "PUSH DE", () => PushReg16("DE"));
            Com(0xE5, 4, 16, "PUSH HL", () => PushReg16("HL"));
            Com(0xF1, 3, 12, "POP AF", () =>
                {
                    PopReg16("AF");
                    Registers.AF.Value &= 0xF0;
                });
            Com(0xC1, 3, 12, "POP BC", () => PopReg16("BC"));
            Com(0xD1, 3, 12, "POP DE", () => PopReg16("DE"));
            Com(0xE1, 3, 12, "POP HL", () => PopReg16("HL"));
            Com(0x87, 1, 4, "ADD A, A", () => AddReg8ToRegA('A'));
            Com(0x80, 1, 4, "ADD A, B", () => AddReg8ToRegA('B'));
            Com(0x81, 1, 4, "ADD A, C", () => AddReg8ToRegA('C'));
            Com(0x82, 1, 4, "ADD A, D", () => AddReg8ToRegA('D'));
            Com(0x83, 1, 4, "ADD A, E", () => AddReg8ToRegA('E'));
            Com(0x84, 1, 4, "ADD A, H", () => AddReg8ToRegA('H'));
            Com(0x85, 1, 4, "ADD A, L", () => AddReg8ToRegA('L'));
            Com(0x86, 2, 8, "ADD A, (HL)", () => AddMemoryByteFromReg16ToRegA("HL"));
            Com(0xC6, 2, 8, "ADD A, n", () => AddNextByteToRegA());
            Com(0x8F, 1, 4, "ADC A, A", () => AddReg8ToRegA('A', true));
            Com(0x88, 1, 4, "ADC A, A", () => AddReg8ToRegA('B', true));
            Com(0x89, 1, 4, "ADC A, A", () => AddReg8ToRegA('C', true));
            Com(0x8A, 1, 4, "ADC A, A", () => AddReg8ToRegA('D', true));
            Com(0x8B, 1, 4, "ADC A, A", () => AddReg8ToRegA('E', true));
            Com(0x8C, 1, 4, "ADC A, A", () => AddReg8ToRegA('H', true));
            Com(0x8D, 1, 4, "ADC A, A", () => AddReg8ToRegA('L', true));
            Com(0x8E, 2, 8, "ADC A, (HL)", () => AddMemoryByteFromReg16ToRegA("HL", true));
            Com(0xCE, 2, 8, "ADC A, n", () => AddNextByteToRegA(true));
            Com(0x97, 1, 4, "SUB A, A", () => SubReg8ToRegA('A'));
            Com(0x90, 1, 4, "SUB A, B", () => SubReg8ToRegA('B'));
            Com(0x91, 1, 4, "SUB A, C", () => SubReg8ToRegA('C'));
            Com(0x92, 1, 4, "SUB A, D", () => SubReg8ToRegA('D'));
            Com(0x93, 1, 4, "SUB A, E", () => SubReg8ToRegA('E'));
            Com(0x94, 1, 4, "SUB A, H", () => SubReg8ToRegA('H'));
            Com(0x95, 1, 4, "SUB A, L", () => SubReg8ToRegA('L'));
            Com(0x96, 2, 8, "SUB A, (HL)", () => SubMemoryByteFromReg16ToRegA("HL"));
            Com(0xD6, 2, 8, "SUB A, n", () => SubNextByteToRegA());
            Com(0x9F, 1, 4, "SBC A, A", () => SubReg8ToRegA('A', true));
            Com(0x98, 1, 4, "SBC A, B", () => SubReg8ToRegA('B', true));
            Com(0x99, 1, 4, "SBC A, C", () => SubReg8ToRegA('C', true));
            Com(0x9A, 1, 4, "SBC A, D", () => SubReg8ToRegA('D', true));
            Com(0x9B, 1, 4, "SBC A, E", () => SubReg8ToRegA('E', true));
            Com(0x9C, 1, 4, "SBC A, H", () => SubReg8ToRegA('H', true));
            Com(0x9D, 1, 4, "SBC A, L", () => SubReg8ToRegA('L', true));
            Com(0x9E, 2, 8, "SBC A, (HL)", () => SubMemoryByteFromReg16ToRegA("HL", true));
            Com(0xDE, 2, 8, "SBC A, n", () => SubNextByteToRegA(true));
            Com(0xA7, 1, 4, "AND A", () => AndReg8ToRegA('A'));
            Com(0xA0, 1, 4, "AND B", () => AndReg8ToRegA('B'));
            Com(0xA1, 1, 4, "AND C", () => AndReg8ToRegA('C'));
            Com(0xA2, 1, 4, "AND D", () => AndReg8ToRegA('D'));
            Com(0xA3, 1, 4, "AND E", () => AndReg8ToRegA('E'));
            Com(0xA4, 1, 4, "AND H", () => AndReg8ToRegA('H'));
            Com(0xA5, 1, 4, "AND L", () => AndReg8ToRegA('L'));
            Com(0xA6, 2, 8, "AND (HL)", () => AndValueToRegA(Memory.ReadByte(Registers["HL"])));
            Com(0xE6, 2, 8, "AND n", () => AndValueToRegA(GetNextByte()));
            Com(0xB7, 1, 4, "OR A", () => OrReg8ToRegA('A'));
            Com(0xB0, 1, 4, "OR B", () => OrReg8ToRegA('B'));
            Com(0xB1, 1, 4, "OR C", () => OrReg8ToRegA('C'));
            Com(0xB2, 1, 4, "OR D", () => OrReg8ToRegA('D'));
            Com(0xB3, 1, 4, "OR E", () => OrReg8ToRegA('E'));
            Com(0xB4, 1, 4, "OR H", () => OrReg8ToRegA('H'));
            Com(0xB5, 1, 4, "OR L", () => OrReg8ToRegA('L'));
            Com(0xB6, 2, 8, "OR (HL)", () => OrValueToRegA(Memory.ReadByte(Registers["HL"])));
            Com(0xF6, 2, 8, "OR n", () => OrValueToRegA(GetNextByte()));
            Com(0xAF, 1, 4, "XOR A", () => XorReg8ToRegA('A'));
            Com(0xA8, 1, 4, "XOR B", () => XorReg8ToRegA('B'));
            Com(0xA9, 1, 4, "XOR C", () => XorReg8ToRegA('C'));
            Com(0xAA, 1, 4, "XOR D", () => XorReg8ToRegA('D'));
            Com(0xAB, 1, 4, "XOR E", () => XorReg8ToRegA('E'));
            Com(0xAC, 1, 4, "XOR H", () => XorReg8ToRegA('H'));
            Com(0xAD, 1, 4, "XOR L", () => XorReg8ToRegA('L'));
            Com(0xAE, 2, 8, "XOR (HL)", () => XorValueToRegA(Memory.ReadByte(Registers["HL"])));
            Com(0xEE, 2, 8, "XOR n", () => XorValueToRegA(GetNextByte()));
            Com(0xBF, 1, 4, "CP A", () => CompareReg8ToRegA('A'));
            Com(0xB8, 1, 4, "CP B", () => CompareReg8ToRegA('B'));
            Com(0xB9, 1, 4, "CP C", () => CompareReg8ToRegA('C'));
            Com(0xBA, 1, 4, "CP D", () => CompareReg8ToRegA('D'));
            Com(0xBB, 1, 4, "CP E", () => CompareReg8ToRegA('E'));
            Com(0xBC, 1, 4, "CP H", () => CompareReg8ToRegA('H'));
            Com(0xBD, 1, 4, "CP L", () => CompareReg8ToRegA('L'));
            Com(0xBE, 2, 8, "CP (HL)", () => CompareValueToRegA(Memory.ReadByte(Registers["HL"])));
            Com(0xFE, 2, 8, "CP n", () => CompareValueToRegA(Memory.ReadByte(GetNextByte())));
            Com(0x3C, 1, 4, "INC A", () => Registers.Inc('A'));
            Com(0x04, 1, 4, "INC B", () => Registers.Inc('B'));
            Com(0x0C, 1, 4, "INC C", () => Registers.Inc('C'));
            Com(0x14, 1, 4, "INC D", () => Registers.Inc('D'));
            Com(0x1C, 1, 4, "INC E", () => Registers.Inc('E'));
            Com(0x24, 1, 4, "INC H", () => Registers.Inc('H'));
            Com(0x2C, 1, 4, "INC L", () => Registers.Inc('L'));
            Com(0x34, 3, 12, "INC (HL)", () => IncreaseMemoryByteFromReg16("HL"));
            Com(0x3D, 1, 4, "DEC A", () => Registers.Dec('A'));
            Com(0x05, 1, 4, "DEC B", () => Registers.Dec('B'));
            Com(0x0D, 1, 4, "DEC C", () => Registers.Dec('C'));
            Com(0x15, 1, 4, "DEC D", () => Registers.Dec('D'));
            Com(0x1D, 1, 4, "DEC E", () => Registers.Dec('E'));
            Com(0x25, 1, 4, "DEC H", () => Registers.Dec('H'));
            Com(0x2D, 1, 4, "DEC L", () => Registers.Dec('L'));
            Com(0x35, 3, 12, "DEC (HL)", () => DecreaseMemoryByteFromReg16("HL"));
            Com(0x09, 2, 8, "ADD HL, BC", () => AddReg16ToReg16("HL", "BC"));
            Com(0x19, 2, 8, "ADD HL, DE", () => AddReg16ToReg16("HL", "DE"));
            Com(0x29, 2, 8, "ADD HL, HL", () => AddReg16ToReg16("HL", "HL"));
            Com(0x39, 2, 8, "ADD HL, SP", () => AddReg16ToReg16("HL", "SP"));
            Com(0xE8, 4, 16, "ADD SP, n", () => AddNextByteToReg16("SP"));
            Com(0x03, 2, 8, "INC BC", () => Registers.Inc("BC"));
            Com(0x13, 2, 8, "INC DE", () => Registers.Inc("DE"));
            Com(0x23, 2, 8, "INC HL", () => Registers.Inc("HL"));
            Com(0x33, 2, 8, "INC SP", () => Registers.Inc("SP"));
            Com(0x0B, 2, 8, "DEC BC", () => Registers.Dec("BC"));
            Com(0x1B, 2, 8, "DEC DE", () => Registers.Dec("DE"));
            Com(0x2B, 2, 8, "DEC HL", () => Registers.Dec("HL"));
            Com(0x3B, 2, 8, "DEC SP", () => Registers.Dec("SP"));
            Com(0x37, 2, 8, "SWAP A", () => SwapNibblesToReg8('A'), true);
            Com(0x30, 2, 8, "SWAP B", () => SwapNibblesToReg8('B'), true);
            Com(0x31, 2, 8, "SWAP C", () => SwapNibblesToReg8('C'), true);
            Com(0x32, 2, 8, "SWAP D", () => SwapNibblesToReg8('D'), true);
            Com(0x33, 2, 8, "SWAP E", () => SwapNibblesToReg8('E'), true);
            Com(0x34, 2, 8, "SWAP H", () => SwapNibblesToReg8('H'), true);
            Com(0x35, 2, 8, "SWAP L", () => SwapNibblesToReg8('L'), true);
            Com(0x36, 4, 16, "SWAP (HL)", () => SwapNibblesToMemoryByteFromReg16("HL"), true);

            Com(0x3F, 1, 4, "CCF", () =>
                {
                    Registers.F['C'] = (byte)(1 - Registers.F['C']);
                    Registers.F['H'] = 0;
                    Registers.F['N'] = 0;
                });
            Com(0x37, 1, 4, "SCF", () =>
            {
                Registers.F['C'] = 1;
                Registers.F['H'] = 0;
                Registers.F['N'] = 0;
            });
            Com(0x2F, 1, 4, "CPL", () =>
                {
                    Registers['A'] = (byte) (~Registers['A']);
                    Registers.F['H'] = 1;
                    Registers.F['N'] = 1;
                });
            Com(0x27, 1, 4, "DAA", () => AdjustDecimalReg8('A'));
            Com(0x07, 1, 4, "RLCA", () => RotateReg8Left('A'));
            Com(0x17, 1, 4, "RLA", () => RotateReg8LeftThroughCarry('A'));

            Com(0x0F, 1, 4, "RRCA", () => RotateReg8Right('A'));
            Com(0x1F, 1, 4, "RRA", () => RotateReg8RightThroughCarry('A'));

            Com(0x76, 1, 4, "HALT", () => Registers.Halt.Value = 1);
            Com(0xF3, 1, 4, "DI", () => Registers.IME.Value = 0);
            Com(0xFB, 1, 4, "EI", () => Registers.IME.Value = 1);

            Com(0xD9, 2, 8, "RETI", ReturnFromInterrupt); // or 3, 12 ? 
        }

        /// <summary>
        /// Returns from interrupt.
        /// </summary>
        private void ReturnFromInterrupt()
        {
            Registers.IME.Value = 1;
            Registers["PC"] = Stack.PopWord();
            if (IntControl.ProcessedInterrupt != Interrupt.None)
            {
                Registers.IF.SetBit((byte) IntControl.ProcessedInterrupt, 0);
                IntControl.ProcessedInterrupt = Interrupt.None;
            }
        }

        /// <summary>
        /// Rotates the reg8 right through carry.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void RotateReg8RightThroughCarry(char reg8)
        {
            byte oldValue = Registers[reg8];
            byte newValue = (byte)((oldValue >> 1) + (Registers.F['C'] == 1 ? 0x80 : 0));
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = (byte)(oldValue % 2);
            Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
        }

        /// <summary>
        /// Rotates the reg8 right.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void RotateReg8Right(char reg8)
        {
            byte oldValue = Registers[reg8];
            byte newValue = (byte) ((oldValue >> 1) + (oldValue % 2 == 1 ? 0x80 : 0));
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = (byte)(oldValue % 2);
            Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
        }

        /// <summary>
        /// Rotates the reg8 left through carry.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void RotateReg8LeftThroughCarry(char reg8)
        {
            byte oldValue = Registers[reg8];
            byte newValue = (byte)((oldValue << 1) + (oldValue > 127 ? 1 : 0));
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = (byte)(oldValue > 127 ? 1 : 0);
            Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
        }

        private void RotateReg8Left(char reg8)
        {
            byte oldValue = Registers[reg8];
            byte newValue = (byte)((oldValue << 1) + Registers.F['C']);
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = (byte)(oldValue > 127 ? 1 : 0);
            Registers.F['Z'] = (byte) (newValue == 0 ? 1 : 0);
        }

        /// <summary>
        /// Adjusts the decimal reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void AdjustDecimalReg8(char reg8)
        {
            byte a = Registers[reg8];

            if (Registers.F['N'] == 0)
            {
                if (Registers.F['H'] == 1 || (a & 0xF) > 9)
                {
                    a += 0x06;
                }
                if (Registers.F['C'] == 1 || a > 0x9F)
                {
                    a += 0x60;
                }
            }
            else
            {
                if (Registers.F['H'] == 1)
                {
                    a = (byte) ((a - 6) & 0xFF);
                }
                if (Registers.F['C'] == 1)
                {
                    a -= 0x60;
                }
            }
            Registers['F'] &= (byte)(~(Registers.F['H'] | Registers.F['Z']));
            if ((a & 0x100) == 0x100)
            {
                Registers.F['C'] = 1;
            }
            a &= 0xFF;
            if (a == 0)
            {
                Registers.F['Z'] = 1;
            }
            Registers[reg8] = a;
        }

        /// <summary>
        /// Swaps the nibbles to memory byte from reg16.
        /// </summary>
        /// <param name="hl">The hl.</param>
        private void SwapNibblesToMemoryByteFromReg16(string hl)
        {
            var input = Memory.ReadByte(Registers[hl]);
            var result = (byte) ((input >> 4) ^ (input << 4));
            Memory.WriteByte(Registers[hl], result);
            Registers.F['Z'] = (byte)(result == 0 ? 1 : 0);
            Registers.F['H'] = 0;
            Registers.F['C'] = 0;
            Registers.F['N'] = 0;
        }

        /// <summary>
        /// Swaps the nibbles to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void SwapNibblesToReg8(char reg8)
        {
            Registers[reg8] = (byte)((Registers[reg8] >> 4) ^ (Registers[reg8] << 4));
            Registers.F['Z'] = (byte)(Registers[reg8] == 0 ? 1 : 0);
            Registers.F['H'] = 0;
            Registers.F['C'] = 0;
            Registers.F['N'] = 0;
        }

        /// <summary>
        /// Adds the next byte to reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void AddNextByteToReg16(string reg16)
        {
            short oldValue = Registers[reg16];
            int summ = oldValue + GetNextByte();
            short newValue = (short)summ;
            Registers.F['H'] = (byte)((oldValue < 256 && newValue >= 256) ? 1 : 0);
            Registers.F['Z'] = 0;
            Registers.F['N'] = 0;
            Registers.F['C'] = (byte)(summ > short.MaxValue ? 1 : 0);
            Registers[reg16] = newValue;
        }

        /// <summary>
        /// Adds the reg16 to reg16.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        private void AddReg16ToReg16(string destination, string source)
        {
            Registers.Add(destination, source);
        }

        /// <summary>
        /// Creates new command with specified opcode, m count, cycles count, name and body.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="m">The m.</param>
        /// <param name="cycles">The cycles count.</param>
        /// <param name="name">The name.</param>
        /// <param name="body">The body.</param>
        /// <param name="extended">if set to <c>true</c> [extended].</param>
        private void Com(int opcode, short m, short cycles, 
            string name, Command.BodyDelegate body, bool extended = false)
        {
            if (extended)
            {
                commandsExtended.Add(opcode, new Command(opcode, m, cycles, name, body));
            }
            else
            {
                commands.Add(opcode, new Command(opcode, m, cycles, name, body));
            }
        }

        #region Helper functions


        /// <summary>
        /// Decreases the memory byte from reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void DecreaseMemoryByteFromReg16(string reg16)
        {
            byte oldValue = Memory.ReadByte(Registers[reg16]);
            int summ = oldValue - 1;
            byte newValue = (byte)summ;
            Registers.F['Z'] = (byte)(newValue == 0 ? 1 : 0);
            Registers.F['N'] = 1;
            Memory.WriteByte(Registers[reg16], newValue);
        }

        /// <summary>
        /// Increases the memory byte from reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        public void IncreaseMemoryByteFromReg16(string reg16)
        {
            byte oldValue = Memory.ReadByte(Registers[reg16]);
            int summ = oldValue + 1;
            byte newValue = (byte) summ;
            Registers.F['Z'] = (byte) (newValue == 0 ? 1 : 0);
            Registers.F['N'] = 0;
            Registers.F['C'] = (byte) (summ > byte.MaxValue ? 1 : 0);
            Memory.WriteByte(Registers[reg16], newValue);
        }

        /// <summary>
        /// Compares the value to reg A.
        /// </summary>
        /// <param name="value">The value.</param>
        private void CompareValueToRegA(byte value)
        {
            Registers.A.Cmp(value);
        }

        /// <summary>
        /// Compares the reg8 to reg A.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void CompareReg8ToRegA(char reg8)
        {
            Registers.A.Cmp(Registers[reg8]);
        }

        /// <summary>
        /// Xors the reg8 to reg A.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void XorReg8ToRegA(char reg8)
        {
            XorValueToRegA(Registers[reg8]);
        }

        /// <summary>
        /// Xors the value to reg A.
        /// </summary>
        /// <param name="value">The value.</param>
        private void XorValueToRegA(byte value)
        {
            Registers['A'] ^= value;
            Registers.F['Z'] = (byte)(Registers['A'] == 0 ? 1 : 0);
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = 0;
        }

        /// <summary>
        /// Ors the reg8 to reg A.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void OrReg8ToRegA(char reg8)
        {
            OrValueToRegA(Registers[reg8]);
        }

        /// <summary>
        /// Ands the value to reg A.
        /// </summary>
        /// <param name="value">The value.</param>
        private void AndValueToRegA(byte value)
        {
            Registers['A'] &= value;
            Registers.F['Z'] = (byte)(Registers['A'] == 0 ? 1 : 0);
            Registers.F['N'] = 0;
            Registers.F['H'] = 1;
            Registers.F['C'] = 0;
        }

        /// <summary>
        /// Ors the value to reg A.
        /// </summary>
        /// <param name="value">The value.</param>
        private void OrValueToRegA(byte value)
        {
            Registers['A'] |= value;
            Registers.F['Z'] = (byte)(Registers['A'] == 0 ? 1 : 0);
            Registers.F['N'] = 0;
            Registers.F['H'] = 0;
            Registers.F['C'] = 0;
        }

        /// <summary>
        /// Ands the reg8 to regA.
        /// </summary>
        /// <param name="source">The source.</param>
        private void AndReg8ToRegA(char source)
        {
            AndValueToRegA(Registers[source]);
        }

        /// <summary>
        /// Moves the reg16 + next byte to reg16.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        private void MoveReg16PlusNextByteToReg16(string source, string destination)
        {
            short oldValue = Registers[source];
            int summ = GetNextByte() + Registers[destination];
            short newValue = (short)summ;
            Registers.F['Z'] = 0;
            Registers.F['N'] = 0;
            Registers.F['H'] = (byte)((oldValue <= 127 && newValue > 127) ? 1 : 0);
            Registers.F['C'] = (byte)(summ > short.MaxValue ? 1 : 0);
            Registers[source] = newValue;
        }

        /// <summary>
        /// Subs the reg8 to reg A.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void SubReg8ToRegA(char reg8, bool withCarry = false)
        {
            Registers.A.Sub(Registers[reg8], withCarry);
        }

        /// <summary>
        /// Moves the reg8 to memory byte from next word.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveReg8ToMemoryByteFromNextWord(char reg8)
        {
            Memory.WriteByte(GetMemoryByteFromNextWord(), Registers[reg8]);
        }

        /// <summary>
        /// Adds the memory byte from reg16 to reg A.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void AddMemoryByteFromReg16ToRegA(string reg16, bool withCarry = false)
        {
            Registers.A.Add(Memory.ReadByte(Registers[reg16]), withCarry);
        }

        /// <summary>
        /// Adds the next byte to reg A.
        /// </summary>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void SubNextByteToRegA(bool withCarry = false)
        {
            Registers.A.Sub(GetNextByte(), withCarry);
        }

        /// <summary>
        /// Subs the memory byte from reg16 to reg A.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void SubMemoryByteFromReg16ToRegA(string reg16, bool withCarry = false)
        {
            Registers.A.Sub(Memory.ReadByte(Registers[reg16]), withCarry);
        }

        /// <summary>
        /// Adds the next byte to reg A.
        /// </summary>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void AddNextByteToRegA(bool withCarry = false)
        {
            Registers.A.Add(GetNextByte(), withCarry);
        }

        public enum AddMode
        {
            NoFlag,
            Inc,
            Dec,
        }

        /// <summary>
        /// Adds the reg8 to reg A.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        /// <param name="withCarry">if set to <c>true</c> [with carry].</param>
        private void AddReg8ToRegA(char reg8, bool withCarry = false)
        {
            Registers.A.Add(Registers[reg8], withCarry);
        }

        /// <summary>
        /// Pushes the reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void PushReg16(string reg16)
        {
            Stack.PushWord(Registers[reg16]);
        }

        /// <summary>
        /// Pops the reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void PopReg16(string reg16)
        {
            Registers[reg16] = Stack.PopWord();
        }

        /// <summary>
        /// Moves the reg16 to memory byte from next word.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void MoveReg16ToMemoryByteFromNextWord(string reg16)
        {
            Memory.WriteWord(GetNextWord(), Registers[reg16]);
        }

        /// <summary>
        /// Moves the next word to reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        /// <returns></returns>
        private short MoveNextWordToReg16(string reg16)
        {
            return Registers[reg16] = GetNextWord();
        }

        /// <summary>
        /// Moves the memory byte from ($FF00 + next byte) to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveMemoryByteFromFF00PlusNextByteToReg8(char reg8)
        {
            Registers[reg8] = Memory.ReadByte(GetAddressFromNextBytePlusFF00());
        }

        /// <summary>
        /// Moves the reg8 to memory byte from ($FF00 + next byte).
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveReg8ToMemoryByteFromFF00PlusNextByte(char reg8)
        {
            Memory.WriteByte(GetAddressFromNextBytePlusFF00(), Registers[reg8]);
        }

        /// <summary>
        /// Moves the next byte to memory byte from reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        private void MoveNextByteToMemoryByteFromReg16(string reg16)
        {
            Memory.WriteByte(Registers[reg16], GetNextByte());
        }

        /// <summary>
        /// Moves the reg8 to memory byte from reg16.
        /// </summary>
        /// <param name="reg16">The reg16.</param>
        /// <param name="reg8">The reg8.</param>
        /// <param name="mode">The mode.</param>
        private void MoveReg8ToMemoryByteFromReg16(string reg16,
                                                   char reg8,
                                                   AddMode mode = AddMode.NoFlag)
        {
            Memory.WriteByte(Registers[reg16], Registers[reg8]);
            switch (mode)
            {
                case AddMode.NoFlag:
                    break;
                case AddMode.Inc:
                    Registers[reg16]++;
                    break;
                case AddMode.Dec:
                    Registers[reg16]--;
                    break;
            }
        }

        /// <summary>
        /// Moves the memory byte from next word to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveMemoryByteFromNextWordToReg8(char reg8)
        {
            Registers[reg8] = GetMemoryByteFromNextWord();
        }

        /// <summary>
        /// Moves the memory byte from F F00 plus C to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveMemoryByteFromFF00PlusCToReg8(char reg8)
        {
            Registers[reg8] = Memory.ReadByte(GetAddressFromFF00PlusC());
        }

        /// <summary>
        /// Moves the reg8 to memory byte from F F00 plus C.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveReg8ToMemoryByteFromFF00PlusC(char reg8)
        {
            Memory.WriteByte(GetAddressFromFF00PlusC(), Registers[reg8]);
        }

        /// <summary>
        /// Moves the next byte to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        private void MoveNextByteToReg8(char reg8)
        {
            Registers[reg8] = GetNextByte();
        }

        /// <summary>
        /// Moves the memory byte from reg16 to reg8.
        /// </summary>
        /// <param name="reg8">The reg8.</param>
        /// <param name="reg16">The reg16.</param>
        /// <param name="mode">The mode.</param>
        private void MoveMemoryByteFromReg16ToReg8(char reg8,
                                                   string reg16,
                                                   AddMode mode = AddMode.NoFlag)
        {
            Registers[reg8] = Memory.ReadByte(Registers[reg16]);
            switch (mode)
            {
                case AddMode.NoFlag:
                    break;
                case AddMode.Inc:
                    Registers[reg16]++;
                    break;
                case AddMode.Dec:
                    Registers[reg16]--;
                    break;
            }
        }

        /// <summary>
        /// Moves the reg8 to reg8.
        /// </summary>
        /// <param name="destinationReg8">The destination reg8.</param>
        /// <param name="sourceReg8">The source reg8.</param>
        private void MoveReg8ToReg8(char destinationReg8, char sourceReg8)
        {
            Registers[destinationReg8] = Registers[sourceReg8];
        }

        /// <summary>
        /// Gets the next byte.
        /// </summary>
        /// <returns></returns>
        private byte GetNextByte()
        {
            byte result = Memory.ReadByte(Registers.PC.Value);
            Registers.PC.Value++;
            return result;
        }

        /// <summary>
        /// Moves the reg16 to reg16.
        /// </summary>
        /// <param name="SourceReg16">The source reg16.</param>
        /// <param name="DestinationReg16">The destination reg16.</param>
        private void MoveReg16ToReg16(string SourceReg16,
                                      string DestinationReg16)
        {
            Registers[SourceReg16] = Registers[DestinationReg16];
        }

        /// <summary>
        /// Gets the address from $FF00 + C.
        /// </summary>
        /// <returns></returns>
        private short GetAddressFromFF00PlusC()
        {
            return (short) ((Registers.C.Value & 0xffff) | 0xFF00);
        }

        /// <summary>
        /// Gets the address from next byte + $FF00.
        /// </summary>
        /// <returns></returns>
        private short GetAddressFromNextBytePlusFF00()
        {
            return (short) ((GetNextByte() & 0xffff) | 0xFF00);
        }

        /// <summary>
        /// Gets the next word.
        /// </summary>
        /// <returns></returns>
        private short GetNextWord()
        {
            byte low = Memory.ReadByte(Registers.PC.Value);
            byte high = Memory.ReadByte((short) (Registers.PC.Value + 1));
            short result = (short) ((low & 0xffff) | (short) (high << 8));
            Registers.PC.Value += 2;
            return result;
        }

        /// <summary>
        /// Gets the memory byte from next word.
        /// </summary>
        /// <returns></returns>
        private byte GetMemoryByteFromNextWord()
        {
            return Memory.ReadByte(GetNextWord());
        }

        #endregion
    }
}