using GBSharpClasses.Enumarations;

namespace GBSharpClasses
{
    public class GPU
    {
        public const byte ScreenHeight = 144;

        public const byte ScreenWidth = 160;

        private int Time { get; set; }

        public GPUMode CurrentMode { get; set; }

        public byte[] vRAM { get; set; }

        public byte[] OAM { get; set; }

        public byte CurrentLine { get; set; }

        public void Reset()
        {
            // TODO : Implement this
        }

        public void Update()
        {
            Time += CPU.Get.Cycles;
            // TODO : Implement this
        }
    }
}