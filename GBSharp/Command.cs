namespace GBSharpClasses
{
    public class Command
    {
        public delegate void BodyDelegate();

        public string Name { get; set; }

        public int Opcode { get; set; }

        public short M { get; set; }

        public short Cycles { get; set; }

        public BodyDelegate Body { get; set; }

        public void Execute()
        {
            CPU.Get.M = M;
            CPU.Get.Cycles = Cycles;
            Body();
        }

        public Command(int opcode, short m, short cycles, string name, BodyDelegate body)
        {
            Opcode = opcode;
            M = m;
            Cycles = cycles;
            Body = body;
            Name = name;
        }
    }
}