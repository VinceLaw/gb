namespace GBSharpClasses.Enumarations
{
    public enum Interrupt
    {
        VBlank = 0,
        LCDC = 1,
        TimerOverflow = 2,
        IOTransferComplete = 3,
        Joypad = 4,
        None = -1,
    }
}