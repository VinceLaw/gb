namespace GBSharpClasses.Enumarations
{
    public enum GPUMode
    {
        OAM = 2,
        VRAM = 3, 
        HBlank = 0,
        VBlank = 1,
    }
}