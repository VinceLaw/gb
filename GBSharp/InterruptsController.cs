using System.Collections.Generic;
using GBSharpClasses.Enumarations;

namespace GBSharpClasses
{
    public class InterruptsController
    {
        private readonly Dictionary<short, Interrupt> 
            interrupts = new Dictionary<short, Interrupt>();

        public Interrupt ProcessedInterrupt { get; set; }

        public InterruptsController()
        {
            ProcessedInterrupt = Interrupt.None;
            interrupts.Add(0x0040, Interrupt.VBlank);
            interrupts.Add(0x0048, Interrupt.LCDC);
            interrupts.Add(0x0050, Interrupt.TimerOverflow);
            interrupts.Add(0x0058, Interrupt.IOTransferComplete);
            interrupts.Add(0x0060, Interrupt.Joypad);
        }

        public void Generate(Interrupt interrupt)
        {
            CPU.Get.Registers.IF.SetBit((byte) interrupt, 1);
        }

        public void Process()
        {
            if (CPU.Get.Registers.IME.Value != 1)
            {
                return;
            }

            foreach (var interrupt in interrupts)
            {
                if (CPU.Get.Registers.IE.GetBit((byte)interrupt.Value) == 1
                    && CPU.Get.Registers.IF.GetBit((byte)interrupt.Value) == 1)
                {
                    CPU.Get.Registers.IME.Value = 0;
                    CPU.Get.Stack.PushWord(CPU.Get.Registers["PC"]);
                    CPU.Get.Registers["PC"] = interrupt.Key;
                    ProcessedInterrupt = interrupt.Value;
                    break;
                }
            }
        }
    }
}