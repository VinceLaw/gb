using System;

namespace GBSharpClasses
{
    public class Timer
    {
        public Int16 Value { get; set; }

        public Int16 MaxValue = Int16.MaxValue;

        public virtual void NextStep()
        {
            MaxValue++;
        }
    }
}